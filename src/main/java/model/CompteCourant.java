package model;

import java.time.LocalDate;

public class CompteCourant extends Compte {

    // Attributs
    private double decouvert;

    // Constructors
    public CompteCourant() {
        super();
    }

    public CompteCourant(int codeCompte, LocalDate dateCreation, double solde, double decouvert, int codeClient) {
        super(codeCompte, dateCreation, solde, codeClient);
        this.decouvert = decouvert;
    }


    // Getters & Setters
    public double getDecouvert() {
        return decouvert;
    }

    public void setDecouvert(double decouvert) {
        this.decouvert = decouvert;
    }
}
