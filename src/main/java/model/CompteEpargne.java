package model;

import java.time.LocalDate;

public class CompteEpargne extends Compte {

    // Attributes
    private double taux;

    // Constructors
    public CompteEpargne() {
        super();
    }

    public CompteEpargne(int codeCompte, LocalDate dateCreation, double solde, double taux, int codeClient) {
        super(codeCompte, dateCreation, solde, codeClient);
        this.taux = taux;
    }

    // Getters & Setteurs
    public double getTaux() {
        return taux;
    }

    public void setTaux(double taux) {
        this.taux = taux;
    }
}
