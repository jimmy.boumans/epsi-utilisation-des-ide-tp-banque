package controller;

import model.Client;
import model.Compte;
import model.CompteCourant;
import model.CompteEpargne;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

public class Operation {

    ArrayList<Client> clients = new ArrayList<Client>();
    ArrayList<Compte> comptes = new ArrayList<>();
    Scanner sc = new Scanner(System.in);
    LocalDate localDate = LocalDate.now();

    public void initClientCompte() {
        clients.add(new Client(1, "aoudjit", "aoudjit@gmail.fr", 1));
        clients.add(new Client(2, "hayri", "Hayri@gmail.com", 2));
        clients.add(new Client(3, "dupont", "Dupont@gmail.com", 3));
        comptes.add(new CompteCourant(1, LocalDate.now(), 100, 300, 1));
        comptes.add(new CompteCourant(2, LocalDate.now(), 10000, 1000, 2));
        comptes.add(new CompteCourant(3, LocalDate.now(), 100000, 1000, 3));
        comptes.add(new CompteEpargne(4, LocalDate.now(), 100000, 0.5, 2));
        comptes.add(new CompteEpargne(5, LocalDate.now(), 1000000, 0.2, 3));
    }

    public void insererClient() {

        String afficheClient;
        FileReader fileClient;
        try {
            fileClient = new FileReader("client.txt");
            BufferedReader lireClient = new BufferedReader(fileClient);
            String[] tab = new String[4];
            while (lireClient.ready()) {
                Client cl = new Client();
                afficheClient = lireClient.readLine();
                tab = afficheClient.split(" ");
                cl.setCode(Integer.parseInt(tab[0]));
                cl.setNom(tab[1]);
                cl.setEmail(tab[2]);
                cl.setCodeCompte(Integer.parseInt(tab[3]));
                clients.add(cl);
            }
            lireClient.close();
            fileClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void insererCompte() {

        String afficheCompte;
        FileReader fileCompte;
        try {
            fileCompte = new FileReader("compte.txt");
            BufferedReader lireCompte = new BufferedReader(fileCompte);
            String[] tab = new String[4];
            while (lireCompte.ready()) {
                Compte cpte = new Compte();
                afficheCompte = lireCompte.readLine();
                tab = afficheCompte.split(" ");
                cpte.setCodeCompte(Integer.parseInt(tab[0]));
                cpte.setDateCreation(LocalDate.parse(tab[1]));
                cpte.setSolde(Double.valueOf(tab[2]));
                cpte.setCodeClient(Integer.parseInt(tab[4]));
                comptes.add(cpte);
            }
            lireCompte.close();
            fileCompte.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Client consulterClient(String nomClient) {

        Client cl = new Client();
        for (Client client : clients) {
            if (client.getNom().equals(nomClient)) {
                cl = client;
                return cl;
            }
        }
        return cl;
    }

    public void creatClient(Client newClient) {

        Compte newCompte = new Compte();

        if (newClient != null) {
            clients.add(newClient);
        } else if (newClient == null) {
            System.out.println("\n***********Création de client***********");
            System.out.print("- Entrer votre nom : ");
            String nomClient = sc.nextLine();
            System.out.print("- Entrer votre E-mail : ");
            String email = sc.nextLine();
            newClient = new Client(clients.size() + 1, nomClient, email, comptes.size() + 1);
            clients.add(newClient);
            creatCompte(newCompte);
            System.out.println("***************Informations");
            System.out.println("Binvenu dans votre banque en ligne "
                    + "\n- Mr/Mme : " + newClient.getNom()
                    + "\n- Code compte N° : " + newClient.getCodeCompte()
                    + "\n- Solde : ");
        }

        FileWriter fileClient;
        PrintWriter enterClient;
        try {
            fileClient = new FileWriter("client.txt");
            enterClient = new PrintWriter(fileClient);

            for (Client client : clients) {
                enterClient.println(client.getCode() + " "
                        + client.getNom() + " " + client.getEmail()
                        + " " + client.getCodeCompte());
            }

            fileClient.close();
            enterClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void creatCompte(Compte cpte) {

        if (cpte != null) {
            comptes.add(cpte);
        } else if (cpte == null) {
            Compte newCompte = new Compte();
            newCompte = new Compte(comptes.size() + 1, localDate.now(), 0, clients.size() + 1);
            comptes.add(cpte);
        }

        FileWriter fileCompte;
        PrintWriter enterCompte;
        try {
            fileCompte = new FileWriter("compte.txt");
            enterCompte = new PrintWriter(fileCompte);

            for (Compte compte : comptes) {
                if (compte instanceof CompteCourant) {
                    enterCompte.println(compte.getCodeCompte() + " "
                            + compte.getDateCreation() + " "
                            + compte.getSolde() + " "
                            + ((CompteCourant) compte).getDecouvert() + " "
                            + compte.getCodeClient());
                } else if (compte instanceof CompteEpargne) {
                    enterCompte.println(compte.getCodeCompte() + " "
                            + compte.getDateCreation() + " "
                            + compte.getSolde() + " "
                            + ((CompteEpargne) compte).getTaux() + " "
                            + compte.getCodeClient());
                }
            }

            fileCompte.close();
            enterCompte.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Compte consulterCompte(String nomClient) {

        Compte cpte = new Compte();
        int codeCompte = 0;
        Client cl = new Client();

        if (consulterClient(nomClient) != null) {
            cl = consulterClient(nomClient);
            codeCompte = cl.getCodeCompte();
            for (Compte compte : comptes) {
                if (compte.getCodeCompte() == codeCompte) {
                    return compte;
                }
            }
        }

        return cpte;
    }

    public String retirer(String nomClient, double montant) throws RuntimeException {

        double faciliteCaisse = 0;
        Compte cpte = consulterCompte(nomClient);

        if (cpte instanceof CompteCourant) {
            faciliteCaisse = ((CompteCourant) cpte).getDecouvert();
        }
        if (cpte.getSolde() + faciliteCaisse < montant) {
            throw new RuntimeException("Solde insuffisant");
        } else {
            cpte.setSolde(cpte.getSolde() - montant);
            return "Solde après opération : " + cpte.getSolde() + "€";
        }
    }

    public String verser(String nomClient, double montant) {

        Compte cpte = consulterCompte(nomClient);
        if (cpte != null) {
            cpte.setSolde(cpte.getSolde() + montant);
            return "Solde après opération : " + cpte.getSolde() + "€";
        }
        return "Opération échouée !";
    }


}
