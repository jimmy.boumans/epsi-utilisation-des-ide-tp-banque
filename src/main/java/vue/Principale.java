package vue;

import controller.Operation;
import model.Client;
import model.Compte;
import model.CompteCourant;
import model.CompteEpargne;

import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Principale {

    public static void main(String[] args) {

        Operation operation = new Operation();

        //********************Initialisation Clients & Comptes********************"
        //operation.initClientCompte();
        operation.insererClient();
        operation.insererCompte();

        System.out.println("\n*******************Bienvenu sur Ma Banque en ligne********************");
        String choixOperation = "";
        do {
            Client cl = new Client();
            Compte cpte = new Compte();
            String creationCompte = "";
            Scanner sc = new Scanner(System.in);
            int montant = 0;
            int saisie = 0;

            System.out.print("Entrer votre nom : ");
            String nomClient = sc.nextLine();
            cl = operation.consulterClient(nomClient);
            if (cl.getNom() == null) {
                System.out.println("\nDésolé vous n'avez pas de compte ! ");
                do {
                    if (saisie != 0) {
                        System.out.println("\nSaisie incorecte !");
                        saisie++;
                    }
                    System.out.print("\nVoulez vous créer un compte ? "
                            + "\n- Créer un compte : taper 1"
                            + "\n- Quitter         : taper 2"
                            + "\n\tVotre choix : ");
                    creationCompte = sc.nextLine();
                    saisie++;
                } while (!creationCompte.equals("1") && !creationCompte.equals("2"));
                saisie = 0;
                if (creationCompte.equals("1")) {
                    do {
                        if (saisie != 0) {
                            System.out.println("\nSaisie incorecte !");
                        }
                        System.out.println("\n***********Choix du compte à créer***********");
                        System.out.print("- Créer un compte courant : taper 1"
                                + "\n- Créer un compte epargne : taper 2"
                                + "\n\tVotre choix : ");
                        creationCompte = sc.nextLine();
                        saisie++;
                    } while (!creationCompte.equals("1") && !creationCompte.equals("2"));
                    saisie = 0;
                    if (creationCompte.equals("1")) {
                        operation.creatClient(null);
                        cpte = new CompteCourant(6, LocalDate.now(), 0, 100, 4);
                        operation.creatCompte(cpte);
                    } else if (creationCompte.equals("2")) {
                        operation.creatClient(null);
                        cpte = new CompteEpargne(6, LocalDate.now(), 0, 0.5, 4);
                        operation.creatCompte(cpte);
                    }
                }
            }
            if (cl.getNom() != null) {
                System.out.println("\n**************************************************"
                        + "\nBonjour Mr/Mm " + cl.getNom() + "\tCompte N°" + cl.getCode()
                        + "\n**************************************************");
                do {
                    do {
                        if (saisie != 0) {
                            System.out.println("\nSaisie incorecte !");
                        }
                        System.out.println("\n***Opérations disponibles sur votre compte***");
                        System.out.print("\n- Consulter le compte   : taper 1"
                                + "\n- Effectuer un retrait  : taper 2"
                                + "\n- Effectuer un verement : taper 3"
                                + "\n\tVotre choix : ");
                        choixOperation = sc.nextLine();
                        saisie++;
                    } while (!choixOperation.equals("1") && !choixOperation.equals("2") && !choixOperation.equals("3"));
                    saisie = 0;
                    if (choixOperation.equals("1")) { // Consultation du compte
                        System.out.println("********Informations********");
                        System.out.println(operation.consulterCompte(nomClient).toString());
                        System.out.println("****************************");
                    } else if (choixOperation.equals("2")) { // Effectuer un retrait
                        System.out.print("Enter le montant à retirer : ");
                        try {
                            montant = sc.nextInt();
                        } catch (InputMismatchException e) {
                            e.printStackTrace();
                            System.out.println("Opération echouée !");
                        }

                        try {
                            System.out.println(operation.retirer(nomClient, montant));
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        }

                    } else if (choixOperation.equals("3")) { // Effectuer un verement
                        System.out.print("Enter le montant à verser : ");
                        montant = sc.nextInt();
                        System.out.println(operation.verser(nomClient, montant));
                    }
                    do {
                        if (saisie != 0) {
                            System.out.println(saisie);
                            System.out.println("\nSaisie incorecte !");
                        }
                        System.out.print("\nVoulez vous effectuer d'autres opération : "
                                + "\n- Pour oui  : Taper 1"
                                + "\n- Pour nous : Taper 2"
                                + "\n\tVotre choix : ");
                        choixOperation = sc.nextLine();
                        saisie = 0;
                    } while (!choixOperation.equals("1") && !choixOperation.equals("2"));
                    saisie = 0;
                } while (choixOperation.equals("1"));
            }
        } while (!choixOperation.equals("2"));

        System.out.println("+--------------------------------------------------------------+"
                + "\n|   Au revoirs et à trés bientôt sur votre banque en ligne !   |"
                + "\n+--------------------------------------------------------------+");


    }
}
