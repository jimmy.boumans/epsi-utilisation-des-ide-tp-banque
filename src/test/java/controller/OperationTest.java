package controller;

import org.junit.Test;

import static org.junit.Assert.*;

public class OperationTest {

    @Test
    public void verserArgentSurCompteOK() {
        Operation operation = new Operation();
        operation.initClientCompte();
        assertEquals("Solde après opération : 1000000.0€", operation.verser("hay", 1000000));
    }

    @Test
    public void retirerAvecSoldeInsuffisantThrowRuntimeException() {
        Operation operation = new Operation();
        operation.initClientCompte();
        Exception exception = assertThrows(RuntimeException.class, () -> {
            operation.retirer("hay", 1000000000);
        });
        String expectedMessage = "Solde insuffisant";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

}
